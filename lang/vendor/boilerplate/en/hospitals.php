<?php

return [
    'title'         => 'Hospitals',
    'returntolist'  => 'Hospital list',
    'save'          => 'Save',
    'informations'  => 'Informations',
    'name'          => 'Name',
    'successadd'    => 'The hospital has been correctly added.',
    'successmod'    => 'The hospital has been correctly modified.',
    'newpassword'   => 'Your password has been saved.',
    'create'        => [
        'title'     => 'Add a hospital',
    ],
    'edit' => [
        'title'     => 'Edit a hospital',
    ],
    'list' => [
        'title'         => 'Hospital list',
        'id'            => 'Id',
        'name'          => 'Name',
        'creationdate'  => 'Creation date',
        'confirmdelete' => 'Do you confirm that you want to delete this hospital ?',
        'deletesuccess' => 'The hospital has been correctly deleted',
        'deleteerror'   => 'An error occured when trying to delete the hospital',
    ],
];
