<?php

return [
    'title'             => 'Rôles',
    'role'              => 'Rôle',
    'parameters'        => 'Paramètres',
    'label'             => 'Libellé',
    'description'       => 'Description',
    'permissions'       => 'Permissions',
    'savebutton'        => 'Enregistrer',
    'successadd'        => 'Le rôle a été correctement ajouté',
    'successmod'        => 'Le role a été correctement modifié',
    'admin'             => [
        'display_name'  => 'Administrateur',
        'description'   => 'Accès total',
        'permissions'   => 'Toutes les permissions',
    ],
    'backend_user' => [
        'display_name'  => 'Utilisateur du back-office',
        'description'   => 'Utilisateurs avec un accès au back-office',
    ],
    'manager' => [
        'display_name'  => 'Gérant',
        'description'   => 'Utilisateurs avec accès gérant',
    ],
    'doctor' => [
        'display_name'  => 'Médecin',
        'description'   => 'Utilisateurs ayant accès à un médecin',
    ],
    'child' => [
        'display_name'  => 'Enfant',
        'description'   => 'Utilisateurs avec accès enfant',
    ],
    'create' => [
        'title'         => 'Ajouter un rôle',
    ],
    'edit' => [
        'title'         => "Édition d'un rôle",
    ],
    'list' => [
        'title'         => 'Liste des rôles',
        'nbusers'       => 'Nb utilisateurs',
        'confirmdelete' => 'Confirmez vous la suppression du rôle ?',
        'deletesuccess' => 'Le rôle a été correctement supprimé',
    ],
];
