<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class RadioOptionsRule implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // Custom validation logic for correct options count
        $correctOptionsCount = collect($value)->filter(function ($option) {
            // Assuming 'correct' values are now '1', '2', or '3'
            return isset($option['correct']) && in_array($option['correct'], ['1', '2', '3']);
        })->count();

        return $correctOptionsCount >= 1;
    }

    /**
     * Get the validation error message.
     *
     * @return string|array
     */
    public function message()
    {
        return 'One meal type must be chosen for each option.';
    }
}

