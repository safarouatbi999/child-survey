<?php

namespace App\Menu;

use Illuminate\Support\Facades\Auth;
use Sebastienheyd\Boilerplate\Menu\Builder;
use Sebastienheyd\Boilerplate\Menu\MenuItemInterface;

class SurveyMenu implements MenuItemInterface
{
    public function make(Builder $menu)
    {
        if (Auth::user()->hasRole(['manager', 'doctor'])) {
            $menu->add('boilerplate::surveys.title', [
                'route' => 'boilerplate.surveys.index',
                'active' => 'boilerplate.surveys.index,boilerplate.surveys.edit,boilerplate.questions.edit',
                'order' => 1030,
            ]);
        }
    }
}
