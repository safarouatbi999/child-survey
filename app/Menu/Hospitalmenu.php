<?php

namespace App\Menu;

use Illuminate\Support\Facades\Auth;
use Sebastienheyd\Boilerplate\Menu\Builder;
use Sebastienheyd\Boilerplate\Menu\MenuItemInterface;

class Hospitalmenu implements MenuItemInterface
{
    public function make(Builder $menu)
    {
        $item = $menu->add('boilerplate::hospitals.title', [
            'permission' => 'users_crud',
            'icon' => 'square',
            'role' => 'admin',
            'order' => 1030,
        ]);

        $item->add('boilerplate::hospitals.create.title', [
            'route' => 'boilerplate.hospitals.create',
            'order' => 1002,
        ]);

        $item->add('boilerplate::hospitals.list.title', [
            'route' => 'boilerplate.hospitals.index',
            'active' => 'boilerplate.hospitals.index,boilerplate.hospitals.edit',
            'order' => 1003,
        ]);
    }
}
