<?php

namespace App\Http\Controllers;

use App\Models\Meal;
use App\Models\Suggestion;
use App\Rules\RadioOptionsRule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class MealController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('meals.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('meals.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $meal = Meal::findOrFail($id);
        return view('meals.edit', [
            'meal' => $meal,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return RedirectResponse
     */

    public function store(Request $request)
    {
        $this->validate($request, [
            'date' => 'required|date|unique:meals,created_at',
            'options' => ['required', 'array', 'min:2', new RadioOptionsRule],
            'options.*.file' => 'required|image|mimes:jpeg,png,gif',
            'options.*.correct' => 'required|in:1,2,3',
            'options.*.option_name' => 'required|string',
        ], [
            'options.required' => 'At least two options are required.',
            'options.array' => 'The options must be an array.',
            'options.min' => 'At least two options are required.',
            'options.*.file.required' => 'The file is required for each option.',
            'options.*.file.image' => 'The file must be an image for each option.',
            'options.*.file.mimes' => 'The file must be a JPEG, PNG, or GIF image for each option.',
            'options.*.correct.required' => 'The meal type is required for each option.',
            'options.*.correct.in' => 'The meal type must be either "Breakfast", "Lunch" or "Dinner" for each option.',
            'options.*.option_name.required' => 'The meal name is required for each option.',
            'options.*.option_name.string' => 'The meal name must be a string for each option.',
        ]);

        $meal = new Meal();
        $meal->created_at = $request->date;
        $meal->save();

        foreach ($request->options as $op) {
            $option = new Suggestion();
            $option->meal_id = $meal->id;
            $option->text = $op['option_name'];
            $option->type = $op['correct'] == '1' ? Suggestion::BREAKFAST : ($op['correct'] == '2' ? Suggestion::LUNCH : Suggestion::DINNER);
            $option->save();
            $file = $op['file'];
            $option->image = $file->storeAs('public/meals/' . $meal->id . '_meal', $option->id . '_suggestion_' . $file->getClientOriginalName());
            $option->image = str_replace('public/', 'storage/app/public/', $option->image);
            $option->save();
        }
        // Set permissions on directories using Storage facade
        Storage::disk('local')->setVisibility('public/meals/' . $meal->id . '_meal', 'public');

        return redirect()->route('boilerplate.meals.edit', $meal)
            ->with('growl', [__('boilerplate::meals.successadd'), 'success']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return RedirectResponse
     */
    public function update($id, Request $request)
    {
        $meal = Meal::findOrFail($id);
        $this->validate($request, [
            'date' => 'required|date|unique:meals,created_at,' . $id,
            'options' => ['required', 'array', 'min:2', new RadioOptionsRule],
            'options.*.correct' => 'required|in:1,2,3',
            'options.*.option_name' => 'required|string',
            'options.*.file' => 'required_without:options.*.old_file|nullable|image|mimes:jpeg,png,gif',
            'options.*.old' => [
                'nullable',
                function ($attribute, $value, $fail) use ($request, $id) {
                    $options = collect($request->input('options'));

                    // Check for duplicate 'old' values within the 'options' array
                    $duplicates = $options->pluck('old')->duplicates()->all();
                    $duplicates = array_filter($duplicates, function ($value) {
                        return $value !== null;
                    });

                    if (!empty($duplicates)) {
                        $fail('Duplicate values are not allowed in the options.');
                    }

                    // Check if the selected option exists for this meal
                    if (!empty($value)) {
                        $optionExists = Suggestion::where('id', $value)
                            ->where('meal_id', $id)
                            ->exists();

                        if (!$optionExists) {
                            $fail('The selected option does not exist for this meal.');
                        }
                    }
                },
            ],
        ], [
            'options.required' => 'At least two options are required.',
            'options.array' => 'The options must be an array.',
            'options.min' => 'At least two options are required.',
            'options.*.correct.required' => 'The correct flag is required for each option.',
            'options.*.correct.in' => 'The meal type must be either "Breakfast", "Lunch" or "Dinner" for each option.',
            'options.*.option_name.required' => 'The option name is required for each option.',
            'options.*.option_name.string' => 'The option name must be a string for each option.',
            'options.*.file.required_without' => 'The file is required for each option.',
            'options.*.file.image' => 'The file must be an image for each option.',
            'options.*.file.mimes' => 'The file must be a JPEG, PNG, or GIF image for each option.',
        ]);

        $oldOptions = collect($request->options)->pluck('old')->all();
        $oldOptions = array_filter($oldOptions, function ($value) {
            return $value !== null;
        });
        if (empty(array_filter($oldOptions))) {
            // If all elements are null, delete all options for the specified meal_id
            $deletedOptions = Suggestion::where('meal_id', $meal->id)->get();
        } else {
            $deletedOptions = Suggestion::where('meal_id', $meal->id)
                ->whereNotIn('id', $oldOptions)->get();
        }

        foreach ($deletedOptions as $option) {
            if (File::exists($option->image)) {
                // Delete old image if it exists
                File::delete($option->image);
            }
            $option->delete();
        }
        $meal->created_at = $request->date;
        $meal->save();

        foreach ($request->options as $op) {
            if (isset($op['old'])) {
                // edit old option
                $option = Suggestion::find($op['old']);
                $option->text = $op['option_name'];
                $option->type = $op['correct'] == '1' ? Suggestion::BREAKFAST : ($op['correct'] == '2' ? Suggestion::LUNCH : Suggestion::DINNER);
                if (isset($op['file'])) {
                    // change image of old option
                    $oldImagePath = $option->image; // Store the old image path
                    if (File::exists($oldImagePath)) {
                        // Delete old image if it exists
                        File::delete($oldImagePath);
                    }
                    $file = $op['file'];
                    $option->image = $file->storeAs('public/meals/' . $meal->id . '_meal', $option->id . '_suggestion_' . $file->getClientOriginalName());
                    $option->image = str_replace('public/', 'storage/app/public/', $option->image);
                }
                $option->save();
            } else {
                //new option
                $option = new Suggestion();
                $option->meal_id = $meal->id;
                $option->text = $op['option_name'];
                $option->type = $op['correct'] == '1' ? Suggestion::BREAKFAST : ($op['correct'] == '2' ? Suggestion::LUNCH : Suggestion::DINNER);
                $option->save();
                $file = $op['file'];
                $option->image = $file->storeAs('public/meals/' . $meal->id . '_meal/', $option->id . '_suggestion_' . $file->getClientOriginalName());
                $option->image = str_replace('public/', 'storage/app/public/', $option->image);
                $option->save();
            }
        }

        return redirect()->route('boilerplate.meals.edit', $meal)
            ->with('growl', [__('boilerplate::meals.successmod'), 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $meal = Meal::findOrFail($id);

        // Get the directory path you want to delete
        $directoryPath = storage_path('app/public/meals/' . $meal->id . '_meal');

        // Check if the directory exists before attempting to delete it
        if (File::exists($directoryPath)) {
            // Delete the directory and its contents
            File::deleteDirectory($directoryPath);
        }
        return response()->json(['success' => $meal->delete() ?? false]);
    }
}
