<?php

namespace App\Http\Controllers;

use App\Models\Option;
use App\Models\Question;
use App\Models\Survey;
use App\Rules\CorrectOptionsRule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class QuestionController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $currentUser = Auth::user();
        if ($currentUser->hasRole('manager')) {
            $question = Question::where('id', $id)->whereHas('survey', function ($q) use ($currentUser) {
                $q->where('manager_id', $currentUser->id);
            })->first();
        } elseif ($currentUser->hasRole('doctor')) {
            $question = Question::where('id', $id)->whereHas('survey', function ($q) use ($currentUser) {
                $q->where('manager_id', $currentUser->manager_id);
            })->first();
        }
        if ($question) {
            return view('surveys.questions.edit', [
                'question' => $question,
            ]);
        }
        abort(403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return RedirectResponse
     */
    public function update($id, Request $request)
    {
        $currentUser = Auth::user();
        if ($currentUser->hasRole('manager')) {
            $question = Question::where('id', $id)->whereHas('survey', function ($q) use ($currentUser) {
                $q->where('manager_id', $currentUser->id);
            })->first();
        } elseif ($currentUser->hasRole('doctor')) {
            $question = Question::where('id', $id)->whereHas('survey', function ($q) use ($currentUser) {
                $q->where('manager_id', $currentUser->manager_id);
            })->first();
        }
        $this->validate($request, [
            'name' => 'required|unique:questions,name,' . $id,
            'options' => ['required', 'array', 'min:2', new CorrectOptionsRule],
            'options.*.correct' => 'required|in:on,off',
            'options.*.option_name' => 'required|string',
            'options.*.file' => 'required_without:options.*.old_file|nullable|image|mimes:jpeg,png,gif',
            'options.*.old' => [
                'nullable',
                function ($attribute, $value, $fail) use ($request, $id) {
                    $options = collect($request->input('options'));

                    // Check for duplicate 'old' values within the 'options' array
                    $duplicates = $options->pluck('old')->duplicates()->all();
                    $duplicates = array_filter($duplicates, function ($value) {
                        return $value !== null;
                    });

                    if (!empty($duplicates)) {
                        $fail('Duplicate values are not allowed in the options.');
                    }

                    // Check if the selected option exists for this question
                    if (!empty($value)) {
                        $optionExists = Option::where('id', $value)
                            ->where('question_id', $id)
                            ->exists();

                        if (!$optionExists) {
                            $fail('The selected option does not exist for this question.');
                        }
                    }
                },
            ],
        ], [
            'options.required' => 'At least two options are required.',
            'options.array' => 'The options must be an array.',
            'options.min' => 'At least two options are required.',
            'options.*.correct.required' => 'The correct flag is required for each option.',
            'options.*.correct.in' => 'The correct flag must be either "on" or "off" for each option.',
            'options.*.option_name.required' => 'The option name is required for each option.',
            'options.*.option_name.string' => 'The option name must be a string for each option.',
            'options.*.file.required_without' => 'The file is required for each option.',
            'options.*.file.image' => 'The file must be an image for each option.',
            'options.*.file.mimes' => 'The file must be a JPEG, PNG, or GIF image for each option.',
        ]);

        if ($question) {
            $oldOptions = collect($request->options)->pluck('old')->all();
            $oldOptions = array_filter($oldOptions, function ($value) {
                return $value !== null;
            });
            if (empty(array_filter($oldOptions))) {
                // If all elements are null, delete all options for the specified question_id
                $deletedOptions = Option::where('question_id', $question->id)->get();
            } else {
                $deletedOptions = Option::where('question_id', $question->id)
                    ->whereNotIn('id', $oldOptions)->get();
            }

            foreach ($deletedOptions as $option) {
                if (File::exists($option->image)) {
                    // Delete old image if it exists
                    File::delete($option->image);
                }
                $option->delete();
            }
            $question->name = $request->name;
            $question->save();

            foreach ($request->options as $op) {
                if (isset($op['old'])) {
                    // edit old option
                    $option = Option::find($op['old']);
                    $option->text = $op['option_name'];
                    $option->correct_answer = $op['correct'];
                    if (isset($op['file'])) {
                        // change image of old option
                        $oldImagePath = $option->image; // Store the old image path
                        if (File::exists($oldImagePath)) {
                            // Delete old image if it exists
                            File::delete($oldImagePath);
                        }
                        $file = $op['file'];
                        $option->image = $file->storeAs('public/surveys/' . $question->survey->id . '_survey/' . $question->id . '_question', $option->id . '_option_' . $file->getClientOriginalName());
                        $option->image = str_replace('public/', 'storage/app/public/', $option->image);
                    }
                    $option->save();
                } else {
                    //new option
                    $option = new Option();
                    $option->question_id = $question->id;
                    $option->text = $op['option_name'];
                    $option->correct_answer = $op['correct'];
                    $option->save();
                    $file = $op['file'];
                    $option->image = $file->storeAs('public/surveys/' . $question->survey->id . '_survey/' . $question->id . '_question', $option->id . '_option_' . $file->getClientOriginalName());
                    $option->image = str_replace('public/', 'storage/app/public/', $option->image);
                    $option->save();
                }
            }

            return redirect()->route('boilerplate.questions.edit', $question)
                ->with('growl', [__('boilerplate::surveys.successmod'), 'success']);
        }
        abort(403);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return RedirectResponse
     */

    public function store($id, Request $request)
    {
        $currentUser = Auth::user();
        if ($currentUser->hasRole('manager')) {
            $survey = Survey::where('id', $id)->where('manager_id', $currentUser->id)->first();
        } elseif ($currentUser->hasRole('doctor')) {
            $survey = Survey::where('id', $id)->where('manager_id', $currentUser->manager_id)->first();
        }
        if ($survey) {
            $this->validate($request, [
                'name' => 'required|unique:questions,name',
                'options' => ['required', 'array', 'min:2', new CorrectOptionsRule],
                'options.*.file' => 'required|image|mimes:jpeg,png,gif',
                'options.*.correct' => 'required|in:on,off',
                'options.*.option_name' => 'required|string',
            ], [
                'options.required' => 'At least two options are required.',
                'options.array' => 'The options must be an array.',
                'options.min' => 'At least two options are required.',
                'options.*.file.required' => 'The file is required for each option.',
                'options.*.file.image' => 'The file must be an image for each option.',
                'options.*.file.mimes' => 'The file must be a JPEG, PNG, or GIF image for each option.',
                'options.*.correct.required' => 'The correct flag is required for each option.',
                'options.*.correct.in' => 'The correct flag must be either "on" or "off" for each option.',
                'options.*.option_name.required' => 'The option name is required for each option.',
                'options.*.option_name.string' => 'The option name must be a string for each option.',
            ]);
            $question = new Question();
            $question->survey_id = $id;
            $question->name = $request->name;
            $question->order = count($survey->questions) + 1;
            $question->type = Question::CheckBox;
            $question->save();

            foreach ($request->options as $op) {
                $option = new Option();
                $option->question_id = $question->id;
                $option->text = $op['option_name'];
                $option->correct_answer = $op['correct'];
                $option->save();
                $file = $op['file'];
                $option->image = $file->storeAs('public/surveys/' . $survey->id . '_survey/' . $question->id . '_question', $option->id . '_option_' . $file->getClientOriginalName());
                $option->image = str_replace('public/', 'storage/app/public/', $option->image);
                $option->save();
            }
            // Set permissions on directories using Storage facade
            Storage::disk('local')->setVisibility('public/surveys/' . $survey->id . '_survey', 'public');
            Storage::disk('local')->setVisibility('public/surveys/' . $survey->id . '_survey/' . $question->id . '_question', 'public');
            return redirect()->route('boilerplate.surveys.edit', $survey)
                ->with('growl', [__('boilerplate::surveys.successadd'), 'success']);
        }
        abort(403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $currentUser = Auth::user();
        if ($currentUser->hasRole('manager')) {
            $question = Question::where('id', $id)->whereHas('survey', function ($q) use ($currentUser) {
                $q->where('manager_id', $currentUser->id);
            })->first();
        } elseif ($currentUser->hasRole('doctor')) {
            $question = Question::where('id', $id)->whereHas('survey', function ($q) use ($currentUser) {
                $q->where('manager_id', $currentUser->manager_id);
            })->first();
        }
        if ($question) {
            // Get the directory path you want to delete
            $directoryPath = storage_path('app/public/surveys/' . $question->survey->id . '_survey/' . $question->id . '_question');

            // Check if the directory exists before attempting to delete it
            if (File::exists($directoryPath)) {
                // Delete the directory and its contents
                File::deleteDirectory($directoryPath);
            }
            return response()->json(['success' => $question->delete() ?? false]);
        }
        abort(403);
    }
}
