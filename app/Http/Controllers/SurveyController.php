<?php

namespace App\Http\Controllers;

use App\Models\Survey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('surveys.list');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $currentUser = Auth::user();
        if ($currentUser->hasRole('manager')) {
            $survey = Survey::where('id', $id)->where('manager_id', $currentUser->id)->first();
        } elseif ($currentUser->hasRole('doctor')) {
            $survey = Survey::where('id', $id)->where('manager_id', $currentUser->manager_id)->first();
        }
        if ($survey) {
            session(['surveyId' => $survey->id]);
            return view('surveys.edit', [
                'survey' => $survey,
            ]);
        }
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return RedirectResponse
     */

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'  => 'required|unique:surveys,name',
            'status'      => 'required|in:0,1',
        ]);
        $currentUser = Auth::user();
        Survey::create([
            'name'  => $request->name,
            'manager_id'  => $currentUser->hasRole('manager') ? $currentUser->id : $currentUser->manager_id,
            'status' =>  $request->status,
        ]);

        return redirect()->route('boilerplate.surveys.index')
            ->with('growl', [__('boilerplate::surveys.successadd'), 'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $currentUser = Auth::user();
        if ($currentUser->hasRole('manager')) {
            $survey = Survey::where('id', $id)->where('manager_id', $currentUser->id)->first();
        } elseif ($currentUser->hasRole('doctor')) {
            $survey = Survey::where('id', $id)->where('manager_id', $currentUser->manager_id)->first();
        }
        if ($survey) {
            return  $survey;
        }
        abort(404);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return RedirectResponse
     */
    public function update($id, Request $request)
    {
        $currentUser = Auth::user();
        if ($currentUser->hasRole('manager')) {
            $survey = Survey::where('id', $id)->where('manager_id', $currentUser->id)->first();
        } elseif ($currentUser->hasRole('doctor')) {
            $survey = Survey::where('id', $id)->where('manager_id', $currentUser->manager_id)->first();
        }
        if ($survey) {
            $rules =  [
                'survey_name' => 'required|unique:surveys,name,' . $id,
                'survey_status'      => 'required|in:0,1',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $surveyNameError = $validator->errors()->first('survey_name');
                $surveyStatusError = $validator->errors()->first('survey_status');
                return redirect()->route('boilerplate.surveys.index')
                    ->with('growl', [$surveyNameError . ' ' . $surveyStatusError, 'error']);
            }
            $survey->name = $request->survey_name;
            $survey->status = $request->survey_status;
            $survey->save();

            return redirect()->route('boilerplate.surveys.index')
                ->with('growl', [__('boilerplate::surveys.successmod'), 'success']);
        }
        abort(403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $currentUser = Auth::user();
        if ($currentUser->hasRole('manager')) {
            $survey = Survey::where('id', $id)->where('manager_id', $currentUser->id)->first();
        } elseif ($currentUser->hasRole('doctor')) {
            $survey = Survey::where('id', $id)->where('manager_id', $currentUser->manager_id)->first();
        }
        if ($survey) {
            // Get the directory path you want to delete
            $directoryPath = storage_path('app/public/surveys/' . $survey->id . '_survey');

            // Check if the directory exists before attempting to delete it
            if (File::exists($directoryPath)) {
                // Delete the directory and its contents
                File::deleteDirectory($directoryPath);
            }
            return response()->json(['success' => $survey->delete() ?? false]);
        }
        abort(403);
    }
}
