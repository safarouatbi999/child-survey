<?php

namespace App\Datatables;

use App\Models\Question;
use Illuminate\Support\Facades\Auth;
use Sebastienheyd\Boilerplate\Datatables\Button;
use Sebastienheyd\Boilerplate\Datatables\Column;
use Sebastienheyd\Boilerplate\Datatables\Datatable;

class QuestionsDatatable extends Datatable
{
    public $slug = 'questions';


    public function datasource()
    {
        $currentUser = Auth::user();
        if ($currentUser->hasRole('manager')) {
            return Question::query()->whereHas('survey', function ($q) use ($currentUser) {
                $q->where('manager_id', $currentUser->id);
                $q->where('id', session('surveyId'));
            });
        } elseif ($currentUser->hasRole('doctor')) {
            return Question::query()->whereHas('survey', function ($q) use ($currentUser) {
                $q->where('manager_id', $currentUser->manager_id);
                $q->where('id', session('surveyId'));
            });
        }
    }

    public function setUp()
    {
        $this->order('id', 'desc')
            // ->buttons('filters', 'csv', 'print')
            ->stateSave();
    }

    public function columns(): array
    {
        $columns = [
            Column::add(__('Name'))
                ->data('name'),

            Column::add(__('Created At'))
                ->width('180px')
                ->data('created_at')
                ->dateFormat(),

            Column::add()
                ->width('20px')
                ->actions(function (Question $question) {
                    return join([
                        Button::edit('boilerplate.questions.edit', $question),
                        Button::delete('boilerplate.questions.destroy', $question)
                    ]);
                }),
        ];

        return $columns;
    }
}
