@extends('boilerplate::layout.index', [
'title' => __('boilerplate::surveys.title'),
'subtitle' => $survey->name,
'breadcrumb' => [
__('boilerplate::surveys.title') => 'boilerplate.surveys.index',
$survey->name
]
])

@section('content')
<form method="POST" action="{{ route('boilerplate.questions.store',$survey->id) }}" autocomplete="off" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-12 pb-3">
            <a href="{{ route("boilerplate.surveys.index") }}" class="btn btn-default" data-toggle="tooltip" title="@lang('boilerplate::surveys.returntolist')">
                <span class="far fa-arrow-alt-circle-left text-muted"></span>
            </a>
            <span class="btn-group float-right">
                <button type="submit" class="btn btn-primary">
                    @lang('boilerplate::surveys.save')
                </button>
            </span>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @component('boilerplate::card', ['title' => 'boilerplate::surveys.add_question'])
            @if($errors->any())
            <div class="alert alert-danger">
                @php
                $displayedErrors = [];
                @endphp
                @foreach ($errors->all() as $error)
                @if (!in_array($error, $displayedErrors) && strpos($error, 'name') !== 0)
                <div>{{ $error }}</div>
                @php
                $displayedErrors[] = $error;
                @endphp
                @endif
                @endforeach
            </div>
            @endif
            @component('boilerplate::input', ['name' => 'name', 'label' => 'boilerplate::surveys.edit.name'])@endcomponent
            <p><button type="button" class="btn btn-info mt-3" id="add-option-btn">@lang('boilerplate::surveys.add_option')</button></p>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="options" role="tabpanel" aria-labelledby="options-tab">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="options-table">
                            <thead>
                                <tr>
                                    <th>@lang('boilerplate::surveys.upload_files')</th>
                                    <th>@lang('boilerplate::surveys.correct_option')</th>
                                    <th>@lang('boilerplate::surveys.option_name')</th>
                                    <th>@lang('boilerplate::surveys.action')</th>
                                </tr>
                            </thead>
                            <tbody id="options-body">
                                <!-- First row -->
                                <tr class="option-row">
                                    <td>
                                        <label for="file_1">@lang('boilerplate::surveys.upload_file')</label>
                                        <input type="file" id="file_1" name="options[1][file]">
                                    </td>
                                    <td>
                                        <div class="form-check">
                                            <input class="form-check-input" type="hidden" value="off" name="options[1][correct]">
                                            <input class="form-check-input" type="checkbox" id="correct_1" name="options[1][correct]">
                                            <label class="form-check-label" for="correct_1">@lang('boilerplate::surveys.correct_option')</label>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" id="option_name_1" name="options[1][option_name]" class="form-control">
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-danger delete-option">Delete</button>
                                    </td>
                                </tr>
                                <!-- Second row -->
                                <tr class="option-row">
                                    <td>
                                        <label for="file_2">@lang('boilerplate::surveys.upload_files')</label>
                                        <input type="file" id="file_2" name="options[2][file]">
                                    </td>
                                    <td>
                                        <div class="form-check">
                                            <input class="form-check-input" type="hidden" value="off" name="options[2][correct]">
                                            <input class="form-check-input" type="checkbox" id="correct_1" name="options[2][correct]">
                                            <label class="form-check-label" for="correct_2">@lang('boilerplate::surveys.correct_option')</label>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" id="option_name_2" name="options[2][option_name]" class="form-control">
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-danger delete-option">Delete</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @endcomponent
        </div>
    </div>
</form>
@component('boilerplate::card', ['title' => 'boilerplate::surveys.list_question'])

@component('boilerplate::datatable', ['name' => 'questions']) @endcomponent

@endcomponent
<script>
    window.onload = function() {
        // Counter for unique IDs
        let optionCounter = 2;

        // Function to add a new option row to the table
        function addOptionRow() {
            optionCounter++;

            let optionRow = `<tr class="option-row">
        <td>
            <label for="file_${optionCounter}">@lang('boilerplate::surveys.upload_file')</label>
            <input type="file" id="file_${optionCounter}" name="options[${optionCounter}][file]">
        </td>
        <td>
            <div class="form-check">
                <input class="form-check-input" type="hidden" value="off" name="options[${optionCounter}][correct]">
                <input class="form-check-input" type="checkbox" id="correct_${optionCounter}" name="options[${optionCounter}][correct]">
                <label class="form-check-label" for="correct_${optionCounter}">@lang('boilerplate::surveys.correct_option')</label>
            </div>
        </td>
        <td>
            <input type="text" id="option_name_${optionCounter}" name="options[${optionCounter}][option_name]" class="form-control">
        </td>
        <td>
            <button type="button" class="btn btn-danger delete-option">Delete</button>
        </td>
        </tr>
        `;

            $('#options-body').append(optionRow);
        }

        // Add option button click event
        $('#add-option-btn').click(function() {
            addOptionRow();
        });

        // Delete option button click event (delegated to handle dynamically added delete buttons)
        $('#options-body').on('click', '.delete-option', function() {
            $(this).closest('.option-row').remove();
        });

        // Add event listener for checkbox change
        document.addEventListener('change', function(event) {
            const checkbox = event.target;
            if (checkbox.type === 'checkbox') {
                checkbox.previousElementSibling.value = checkbox.checked ? 'on' : 'off';
            }
        });

    };
</script>
@endsection