@extends('boilerplate::layout.index', [
'title' => __('boilerplate::hospitals.title'),
'subtitle' => __('boilerplate::hospitals.list.title'),
'breadcrumb' => [
__('boilerplate::hospitals.list.title') => 'boilerplate.hospitals.index'
]
])

@section('content')
<div class="row">
    <div class="col-12 mbl">
        <span class="float-right pb-3">
            <a href="{{ route("boilerplate.hospitals.create") }}" class="btn btn-primary">
                @lang('boilerplate::hospitals.create.title')
            </a>
        </span>
    </div>
</div>

@component('boilerplate::card')

@component('boilerplate::datatable', ['name' => 'hospitals']) @endcomponent

@endcomponent
@endsection

@push('css')
<style>
    .img-circle {
        border: 1px solid #CCC
    }
</style>
@endpush