@extends('boilerplate::auth.layout', ['title' => __('boilerplate::games.index')])

@section('content')
<style>
    body {
        background-image: url('{{ asset("assets/background/01.jpg") }}');
        background-size: cover;
        background-position: center;
    }

    .survey-card {
        height: 100px;
        background-color: #ffffff;
        border-radius: 10px;
        box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        padding: 20px;
        display: flex;
        justify-content: center;
        align-items: center;
        transition: border-color 0.3s ease;
        text-align: center;
    }

    .survey-card:hover {
        border: 2px solid #007bff;
    }

    .survey-card .card-title {
        margin-bottom: 0;
    }

    .survey-link {
        text-decoration: none;
        color: inherit;
        width: 100%;
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .survey-card-meals:hover {
        border: 2px solid #28a745;
    }

    .survey-card-meals:hover .card-title {
        color: #28a745;
    }

    .logo-container {
        text-align: center;
        margin-bottom: 20px;
    }

    .logo-container img {
        max-width: 150px;
    }

    .outer-container {
        margin-left: 10px; /* Adjust as needed */
        margin-right: 10px; /* Adjust as needed */
    }

    .container {
        box-shadow: 0 4px 6px rgba(0, 0, 0, 0.5);
        padding: 20px;
        background: rgb(248 248 235 / 70%);
        border-radius: 10px;
    }

    .image-container {
        background-color: #ffffff;
        box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);
        padding: 10px;
        border-radius: 8px;
        text-align: center;
        margin-bottom: 10px;
        /* Added margin-bottom for spacing */
    }

    .image-container img {
        max-width: 100%;
        height: 100px;
        display: block;
        margin: 0 auto;
    }

</style>

<div class="outer-container">
    <div class="container mt-5">
        <div class="logo-container">
            <img src="{{ asset('assets/background/logo.png') }}" alt="Logo">
            <h2>D'trip! مرحباً بأبطال الإكتشاف! إنطلقوا في رحلة إستكشاف جديدة ومليئة بالمرح مع </h2>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-4 col-md-6 col-sm-12 mb-4">
                <div class="card survey-card survey-card-meals">
                    <a href="{{ route('frontoffice.meals') }}" class="survey-link">
                        <h5 class="card-title">الوجبات</h5>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 mb-4">
                <div class="card survey-card">
                    <a href="{{ route('frontoffice.games') }}" class="survey-link">
                        <h5 class="card-title">الألعاب</h5>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="mt-5">
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <div class="image-container">
                    <img src="{{ asset('assets/background/s_1.jpeg') }}" alt="Image 1">
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="image-container">
                    <img src="{{ asset('assets/background/s_2.jpg') }}" alt="Image 2">
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="image-container">
                    <img src="{{ asset('assets/background/s_3.png') }}" alt="Image 3">
                </div>
            </div>
        </div>
        <br>
    </div>
</div>
@endsection