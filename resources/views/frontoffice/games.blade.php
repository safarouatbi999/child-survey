@extends('boilerplate::auth.layout', ['title' => __('boilerplate::games.title')])

@section('content')
<style>
     body {
        background-image: url('{{ asset("assets/background/03.jpg") }}');
        background-size: cover;
        background-position: center;
    }
    .survey-card {
        height: 100px;
        background-color: #ffffff;
        border-radius: 10px;
        box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        padding: 20px;
        display: flex;
        justify-content: center;
        align-items: center;
        transition: border-color 0.3s ease;
        text-align: center;
        /* Center text horizontally */
    }

    .survey-card:hover {
        border: 2px solid #007bff;
    }

    .survey-card .card-title {
        margin-bottom: 0;
    }

    .survey-link {
        text-decoration: none;
        color: inherit;
        /* Inherit text color from parent */
        width: 100%;
        /* Ensure the link takes up the entire card */
        height: 100%;
        /* Ensure the link takes up the entire card */
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .back-btn {
        display: inline-block;
        padding: 8px 16px;
        margin-bottom: 20px;
        background-color: #007bff;
        color: #fff;
        text-decoration: none;
        border: none;
        border-radius: 5px;
        cursor: pointer;
        transition: background-color 0.3s ease;
    }

    .back-btn:hover {
        background-color: #0056b3;
        color: #fff;
    }
    .container {
        box-shadow: 0 4px 6px rgba(0, 0, 0, 0.5);
        padding: 20px;
        background:rgb(248 248 235 / 70%);
        border-radius: 10px;
    }
</style>

<div class="container mt-5">
    <a href="{{ url('/') }}" class="back-btn"><i class="fas fa-arrow-left"></i></a>
    <div class="row justify-content-center">
        @foreach($surveys as $survey)
        <div class="col-lg-4 col-md-6 col-sm-12 mb-4">
            <div class="card survey-card">
                <a href="{{ route('frontoffice.create', ['survey' => $survey]) }}" class="survey-link">
                    <h5 class="card-title">{{$survey->name}}</h5>
                </a>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection