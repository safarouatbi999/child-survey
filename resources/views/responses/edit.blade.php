@extends('boilerplate::layout.index', [
'title' => __('boilerplate::surveys.responses.title'),
'subtitle' => $submission->id,
'breadcrumb' => [
__('boilerplate::surveys.responses.list.title') => 'boilerplate.responses.index',
$submission->id
]
])

@section('content')
<div class="row">
    <div class="col-12 pb-3">
        <a href="{{ route("boilerplate.responses.index") }}" class="btn btn-default" data-toggle="tooltip" title="@lang('boilerplate::surveys.responses.list.title')">
            <span class="far fa-arrow-alt-circle-left text-muted"></span>
        </a>
    </div>
</div>
@component('boilerplate::card', ['title' => __('boilerplate::surveys.response_survey')])
    <p>Child: {{$submission->child_name}}</p>
    
    <ul class="nav nav-tabs" id="responseTabs" role="tablist">
        @foreach($responses as $key => $question)
            <li class="nav-item">
                <a class="nav-link @if($key === 0) active @endif" id="tab{{$key}}" data-toggle="tab" href="#panel{{$key}}" role="tab" aria-controls="panel{{$key}}" aria-selected="@if($key === 0) true @else false @endif">{{$question[0]->question->name}}</a>
            </li>
        @endforeach
    </ul>
    
    <div class="tab-content" id="responseTabContent">
        @foreach($responses as $key => $question)
            <div class="tab-pane fade @if($key === 0) show active @endif" id="panel{{$key}}" role="tabpanel" aria-labelledby="tab{{$key}}">
                @foreach($question as $q)
                    <div class="response-option">
                        <p>{{$q->option->text}}</p>
                        @if($q->option->image)
                            <img src="{{ asset($q->option->image) }}" width="100px">
                        @endif
                    </div>
                @endforeach
            </div>
        @endforeach
    </div>
@endcomponent
@endsection