<?php

namespace Database\Seeders;

use App\Models\Hospital;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class HospitalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Hospital::create([
            "name" => 'Hospital Sahloul',
        ]);
        Hospital::create([
            "name" => 'Hospital Farhat Hachad',
        ]);
    }
}
