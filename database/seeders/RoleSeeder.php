<?php

namespace Database\Seeders;

use App\Models\Boilerplate\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // edit backend role to manager
        $manager_role = Role::find(2);
        $manager_role->name = 'manager';
        $manager_role->display_name = 'boilerplate::role.manager.display_name';
        $manager_role->description = 'boilerplate::role.manager.description';
        $manager_role->save();

        // add doctor role
        $doctor_role = new Role();
        $doctor_role->name = 'doctor';
        $doctor_role->display_name = 'boilerplate::role.doctor.display_name';
        $doctor_role->description = 'boilerplate::role.doctor.description';
        $doctor_role->save();

        Role::find(3)->permissions()->sync([1]); // doctor role can Access to the back office

        // add child role
        $child_role = new Role();
        $child_role->name = 'child';
        $child_role->display_name = 'boilerplate::role.child.display_name';
        $child_role->description = 'boilerplate::role.child.description';
        $child_role->save();
        
        Role::find(4)->permissions()->sync([1]); // child role can Access to the back office

    }
}
